FROM gradle:5.4.1-jdk8-alpine as build
ARG GRADLETASK=build
WORKDIR /home/gradle
ENV GRADLE_USER_HOME /home/gradle
COPY --chown=gradle:gradle build.gradle /home/gradle
RUN gradle --info $GRADLETASK -g/home/gradle || return 0
COPY --chown=gradle:gradle . /home/gradle
RUN gradle --info $GRADLETASK -g/home/gradle

FROM openjdk:8-jre-slim
COPY --from=build /home/gradle/build/libs/java.app.jar /java.app/
WORKDIR /java.app
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "java.app.jar"]
