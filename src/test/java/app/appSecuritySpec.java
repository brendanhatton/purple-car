package app;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@DisplayName("Security unit tests")
@WebMvcTest
//@Tag("security")
public class appSecuritySpec {

    @Autowired
    private MockMvc mockMvc;

    Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
    Set<ConstraintViolation<Car>> constraintViolations;

    Vin VALID_VIN = new Vin("AAAAAAAAAAAAAAAAA");

    @Test
    public void should_throwException_whenVinInvalid() {
        String[] failingValues = {null, "", " ",
                "                 ",
                VALID_VIN + "X",
                "AAAAAAAAAAAAAAAAO",
                "AAAAAAAAAAAAAAAAI",
                "AAAAAAAAAAAAAAAAQ",
        };

        for (String vin : failingValues) {
            constraintViolations = validator.validate(new Car(new CarId(1), new Vin(vin), "make", "model"));
            assertFalse(constraintViolations.isEmpty());
        }

        constraintViolations = validator.validate(new Car(new CarId(1), VALID_VIN, "make", "model"));
        assertTrue(constraintViolations.isEmpty());

    }

    @Test
    public void should_throwException_whenCarIDInvalid() {
        Integer[] failingValues = {-1, 0, null};

        for (Integer carId : failingValues) {
            constraintViolations = validator.validate(new Car(new CarId(carId), VALID_VIN, "make", "model"));
            assertFalse(constraintViolations.isEmpty());
        }

        constraintViolations = validator.validate(new Car(new CarId(1), VALID_VIN, "make", "model"));
        assertTrue(constraintViolations.isEmpty());
        constraintViolations = validator.validate(new Car(new CarId(Integer.MAX_VALUE), VALID_VIN, "make", "model"));
        assertTrue(constraintViolations.isEmpty());
    }

    @Test
    public void should_throwException_whenMakeInvalid() {
        String[] failingValues = {null, "", " "};

        for (String make : failingValues) {
            constraintViolations = validator.validate(new Car(new CarId(1), VALID_VIN, make, "model"));
            assertFalse(constraintViolations.isEmpty());
        }

    }

    @Test
    public void should_throwException_whenModelInvalid() {
        String[] failingValues = {null, "", " "};

        for (String model : failingValues) {
            constraintViolations = validator.validate(new Car(new CarId(1), VALID_VIN, "make", model));
            assertFalse(constraintViolations.isEmpty());
        }

    }

}
