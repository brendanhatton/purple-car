package app;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;

import javax.validation.Valid;
import java.util.ArrayList;

@RestController
public class CarSaleController {

    /**
     * A sudo storage to save Cars
     */
    private ArrayList<Car> storage = new ArrayList<Car>();

    @RequestMapping("/")
    public String index() {
        return "Welcome to The Purple Car, an online car sale company";
    }

    @RequestMapping("/add")
    public String addCar(@Valid @RequestBody Car car) {
//        this.storage.add(car);
        return "Added car: " + car.toString();
    }
//    @RequestMapping("/test")
//    public String addCarTest(@Valid @RequestBody CarTest car) {
////        this.storage.add(car);
//        return "Added car: " + car.toString();
//    }

    @RequestMapping("/view/{id}")
    public String viewCarById(@PathVariable("id") Integer id){
        try {
            return this.storage.get(id).toString();
        } catch (IndexOutOfBoundsException e) {
            return "Sorry! I cannot find this car";
        }
    }

    @RequestMapping("/bob")
    public String addBob(@Valid @RequestBody Bob bob) {
//        this.storage.add(bob);
        return "Added car: " + bob.toString();
    }
}
