package app;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Objects;

public class CarId {

    @NotNull
    @Positive
    private final Integer carId;

    CarId(String carId) {
        this(Integer.parseInt(carId));
    }

    CarId(Integer carId) {
        this.carId = carId;
    }

    public Integer getCarId() {
        return carId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CarId carId1 = (CarId) o;
        return Objects.equals(carId, carId1.carId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(carId);
    }

    @Override
    public String toString() {
        return "CarId{" +
                "carId=" + carId +
                '}';
    }
}
