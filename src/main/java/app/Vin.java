package app;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.Objects;

public class Vin {

    @NotBlank
    @Length(min = 17, max = 17)
    @Pattern(regexp = "[0-9A-Z&&[^IOQ]]{17}")
    private String vin;

    Vin(String vin) {
        this.vin = vin;
    }

    public String getVin() {
        return vin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vin vin1 = (Vin) o;
        return Objects.equals(vin, vin1.vin);
    }

    @Override
    public int hashCode() {
        return Objects.hash(vin);
    }
}
