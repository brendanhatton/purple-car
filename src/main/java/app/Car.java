package app;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

/**
 * Apache Commons Validator package has methods for
 * various validations: https://commons.apache.org/proper/commons-validator/
 */

public class Car {

    /**
     * A local unique identifier
     */
    @Valid
    private CarId carID;

    /**
     * North America VIN standard 1981
     */
    @Valid
    private Vin vin;

    /**
     * Brand of the car (e.g. Audi)
     */
    @NotBlank
    private String make;

    /**
     * A name used by manufacture to market a range
     * of similar cars (e.g. Q5)
     */
    @NotBlank
    private String model;

    /**
     * Vehicle Registration Number Sensitive and unique.
     * private String vrn;
     */

    /**
     * Photo of the car.
     * private File photo;
     */

    public Car(CarId carID, Vin vin, String make, String model) {
        this.carID = carID;
        this.vin = vin;
        this.make = make;
        this.model = model;
    }

    public CarId getCarID() {
        return this.carID;
    }

    public Vin getVin() {
        return this.vin;
    }

    public String getMake() {
        return this.make;
    }

    public String getModel() {
        return this.model;
    }

    @Override
    public String toString() {
        return this.carID.toString() + " " + this.vin + " " + this.make + " " + this.model;
    }

}
