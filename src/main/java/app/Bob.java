package app;

public class Bob {
    private String name;

    public Bob(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
